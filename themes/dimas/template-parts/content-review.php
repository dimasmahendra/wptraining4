<header id='review-head'>
  <h1><?php the_title() ?></h1>
   <table id="tabelTesti" class="wp-list-table widefat fixed striped pages" cellspacing="0" border="1" width="100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Position</th>  
                <th>Email</th>  
                <th>Phone Number</th>  
                <th>Website</th>  
                <th>Images</th>       
              </tr>
            </thead>
            <tbody>
                <?php 
                  $no = 1;
                  $post_meta = get_post_meta(get_the_ID());
                ?>
                <tr align="center">
                    <td><?php echo $no;$no++; ?></td>
                    <td><?php echo $post_meta['prefix-position'][0]; ?></td> 
                    <td><?php echo $post_meta['prefix-email'][0]; ?></td> 
                    <td><?php echo $post_meta['prefix-phone_number'][0]; ?></td> 
                    <td><?php echo $post_meta['prefix-webiste'][0]; ?></td> 
                    <td><img src="<?php echo wp_get_attachment_url( $post_meta['prefix-image_advanced_6'][0] ); ?>" width="100px" height="100px"></td> 
                </tr>        
            </tbody>              
        </table>
</header>