<?php get_header(); ?>
  <div class="row">
    <div class="col-sm-8 blog-main">
      <?php get_template_part( 'template-parts/content', 'review' ); ?>
    </div> <!-- /.blog-main -->
    <?php get_sidebar(); ?>
  </div> <!-- /.row -->
<?php get_footer(); ?>
