<?php get_header(); ?>
  <div class="row">
    <div class="col-sm-8 blog-main">
      <?php
        // Start the loop.
        while ( have_posts() ) : the_post();
 
            // Include the page content template.
            get_template_part( 'content', 'page' );
 
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) {
                comments_template();
            }
 
            // End of the loop.
        endwhile;
        ?>
    </div> <!-- /.blog-main -->
    <?php get_sidebar(); ?>
  </div> <!-- /.row -->
<?php get_footer(); ?>