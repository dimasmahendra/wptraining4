<?php 

function register_my_menus() {
  register_nav_menus(
    array(
      'primary' => __( 'Primary Navigation'),
      'secondary' => __('Secondary Navigation')
    )
  );
}
add_action( 'init', 'register_my_menus' );

function dimas_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar.'),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'dimas_widgets_init' );

function your_prefix_get_meta_box( $meta_boxes ) {
	$prefix = 'prefix-';

	$meta_boxes[] = array(
		'id' => 'untitled',
		'title' => esc_html__( 'Other information', 'metabox-team-member' ),
		'post_types' => array( 'post', 'page', 'team_members'),
		'context' => 'advanced',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'position',
				'type' => 'text',
				'name' => esc_html__( 'Position', 'metabox-team-member' ),
			),
			array(
				'id' => $prefix . 'email',
				'name' => esc_html__( 'Email', 'metabox-team-member' ),
				'type' => 'email',
			),
			array(
				'id' => $prefix . 'phone_number',
				'type' => 'number',
				'name' => esc_html__( 'Phone Number', 'metabox-team-member' ),
			),
			array(
				'id' => $prefix . 'webiste',
				'type' => 'url',
				'name' => esc_html__( 'URL', 'metabox-team-member' ),
			),
			array(
				'id' => $prefix . 'image_advanced_6',
				'type' => 'image_advanced',
				'name' => esc_html__( 'Image', 'metabox-team-member' ),
			),
		),
	);

	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'your_prefix_get_meta_box' );
?>