<?php
/*
Plugin Name: Team Members
Description: Simple Team Members List
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
add_shortcode('test', 'team_member_page');

function team_member_page(){
	global $wpdb;
  $post = $wpdb->get_results( "SELECT * FROM `wp_latihanposts` WHERE post_type like '%team_members%' AND post_title not like '%Auto%Draft%' AND post_status like '%publish%'", ARRAY_A);
  foreach ($post as $key => $value) {
    $post_meta[] = get_post_meta($value['ID']); 
  }   
  ?>
  <div class="wrap">
      <h2>Team Member</h2>
        <table id="tabelTesti" class="wp-list-table widefat fixed striped pages" cellspacing="0" border="1" width="100%">
            <thead>
              <tr>
                <th>No</th>
                <th>Position</th>  
                <th>Email</th>  
                <th>Phone Number</th>  
                <th>Website</th>  
                <th>Images</th>       
              </tr>
            </thead>
            <tbody>
                <?php
                  $no = 1;            
                  foreach ($post_meta as $row)
                  { ?>
                        <tr align="center">
                            <td><?php echo $no;$no++; ?></td>
                            <td><?php echo $row['prefix-position'][0]; ?></td> 
                            <td><?php echo $row['prefix-email'][0]; ?></td> 
                            <td><?php echo $row['prefix-phone_number'][0]; ?></td> 
                            <td><?php echo $row['prefix-webiste'][0]; ?></td> 
                            <td><img src="<?php echo wp_get_attachment_url( $row['prefix-image_advanced_6'][0] ); ?>" width="100px" height="100px"></td> 
                        </tr>
                <?php } ?>            
            </tbody>              
        </table>
    </div>
<?php }
?>