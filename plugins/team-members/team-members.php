<?php
/*
Plugin Name: Team Members
Description: Custom post type for Team Member.
Version: 1.0
Author: Dimas Mahendra Kusuma
*/
add_action( 'init', 'create_team_member' );

function create_team_member() {
    register_post_type( 'team_members',
        array(
            'labels' => array(
                'name' => 'Team Members',
                'singular_name' => 'Team Members',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Team Members',
                'edit' => 'Edit',
                'edit_item' => 'Edit Team Members',
                'new_item' => 'New Team Members',
                'view' => 'View',
                'view_item' => 'View Team Members',
                'search_items' => 'Search Team Members',
                'not_found' => 'No Team Members Reviews found',
                'not_found_in_trash' => 'No Team Members found in Trash',
                'parent' => 'Parent Team Members'
            ),
 
            'public' => true,
            'menu_position' => 15,
            'supports' => array( 'title', 'editor', 'comments', 'thumbnail'),
            'taxonomies' => array( '' ),
            'menu_icon' => plugins_url( 'images/movie.png', __FILE__ ),
            'has_archive' => true
        )
    );
}
?>